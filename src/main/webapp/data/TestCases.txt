Status Codes for External Links : statusCodeExternalLinks
Status Codes for Internal Links : statusCodeInternalLinks
Link Response Time : linkResponseTime
Image Size : imageSize
Over Character H1 Tags : overCharacterH1Tags
Over Character H2 Tags : overCharacterH2Tags
Content And HTML Ratio : contentAndHTMLRatio
Internal Outgoing Links : internalOutgoingLinksCount
External Outgoing Links : externalOutgoingLinksCount
Title Minimum Length : titleMinimumLength
Title Maximum Length : titleMaximumLength
Meta Description Length : descriptionLength
Missing Meta Description : missingDescription
Multiple Meta Description : multipleDescription
Blank Canonical URL : blankCanonicalURL
Missing Canonical URL : missingCanonicalTags
Multiple Canonical Tags : multipleCanonicalTags
Div instead of Table : useDivInsteadOfTable
Duplicate Meta Description : duplicateDescription
Duplicate Body Content : duplicateBodyContent
Anchor External Link : anchorExtenalLink
Google Page Speed : pageSpeed
OG Tags : OGTags
Schema Markup Validation : structuredData
NOODP Tags : NOODPTags
NOYDIR Tags : NOYDIRTags
NO INDEX Tags : NOINDEXTags
NO FOLLOW Tags : NOFOLLOWTags
HREF Language Tags : HREFLanguageTags
Multiple H1 Tags : multipleH1Tags
Missing H1 Tags : missingH1Tags
Blank H1 Tags : blankH1Tags
Blank H2 Tags : blankH2Tags
Image Alt Text : imageAltText
Image Title Text : imageTitleText
Image Source : imageSRC
Missing Title : missingTitle
Multiple Title : multipleTitle
Blank Meta Description : blankDescription
Canonical URL Length : canonicalLength
No Frames : noFrames
No Flash : noFlash
Page Depth : pageDepth
Duplicate Titles : duplicateTitle
Duplicate H1 Tags : duplicateH1Tag
Anchor Link Text : anchorLinkText
Broken Links in SiteMap.xml : brokenLinksSitemapXML
Missing Links in Sitemap.xml : missingLinksInSitemapXML
Verify Robot.txt : verifyRobotsTXT
Verify Sitemap.xml : verifySitemapXML